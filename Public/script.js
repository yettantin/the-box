// Précise que le js est chargé qu'un fois le html chargé
$(document).ready(function(){

    ///////////////////   PAGE INDEX    ////////////////////////////////////////////////////////////////////////////

    // Fonction du bouton "se connecter" de la page d'index

        $("#connect").click(function(){
                //charge dans EmplacementFormaulaire (index.html) la partie Connexion de formulaire.html
            $("#emplacementFormulaire").load("../../../../public/formulaires.html #connexion", function(){
                // #EmplacementFormulaire = div vide de la page index.html
                // #Connexion = le formulaire de connexion dans formulaires.html 
                
                $("form").submit(function(event){

                        // supprime l'évennement du submit par défaut : bloque l'action par défaut du formulaire sans quoi l'attribution d'une nouvelle action ne fonctionnerais pas 
                        event.preventDefault()  
                        
                    //let userConnection = $('#username').val();
                    //let passConnection = $('#password').val();                    
                    
                        // Attribue au formulaire l'action. 
                    $("form").attr("action",window.location.origin +"/cgi-bin/TheBox.exe/connect");
                        //window.location.origin = http://localhost
                        
                        // "Réactive/débloque" le submit 
                    $('form').unbind('submit').submit();
                })
            })    
        })
    
    // Fonction du bouton "Creer un compte" de la page d'index 
    
        $("#create").click(function(){
            $("#emplacementFormulaire").load("../../../../public/formulaires.html #creation", function(){  
                $("form").submit(function(event){    
                    event.preventDefault()
                    $("form").attr("action",window.location.origin +"/cgi-bin/TheBox.exe/createUser");
                   
                        // Test (après click) les mots de passes renseignés avant envoi au serveur
                    if ($("#password").val() !== $("#confirmPassword").val()) {
                        alert("Veuillez entrer des mots de passe égaux");

                            // Bloque la nouvelle action du submit
                        event.preventDefault();
                        return false;                    
                    }
                    $('form').unbind('submit').submit();
                })    
            })           
        })
    
    ////////////////////////  PAGE USER   /////////////////////////////////////////////////////////////////

    // Crée la fenêtre du clic droit de l'arborescence pour ajouter un nouveau dossier
    $(function() {
        $("#alert").dialog({
            //  #alert = div de user.html
            //  .dialog = génère une fenêtre pop up dialogue; méthode jQuery.UI
            autoOpen: false,
            show: {
                effect: "explode",
                duration: 200
            },
            hide: {
                effect: "explode",
                duration: 200
            }
        })
    })
    
        // Récupère le nom de l'id de l'utilisateur en cours
    let parametre = window.location.search;
        // window.location.search = ?paramètre=valeur

    // Evennement du bouton "modifier compte"
    $("#modifBtn").click(function(){
        $("#arborescence").load("../../../../public/formulaires.html #compteUser", function(){
            
            //---   MODIFIER COMPTE   ---------------------------------------------------------
            $("form").submit(function(event){
                event.preventDefault()         
                $("form").attr("action",window.location.origin +"/cgi-bin/TheBox.exe/updateUser" + parametre);       
                if ($("#newPassword").val() !== $("#confirmNewPassword").val()) {       
                    alert("Veuillez entrer des mots de passe egaux");
                    event.preventDefault();
                    return false;
                }
                $('form').unbind('submit').submit();                                           
            })

            //---   SUPPRIMER COMPTE   --------------------------------------------------------                           
            $("#deleteBtn").on("click",function(){                        
                window.location = window.location.origin + "/cgi-bin/TheBox.exe/deleteUser" + parametre;  
            })
        })
    })

    //---------------------------------------------------------------------------------------------------------------------
    // Evennement du bouton "fichiers"
    $("section").on("click", "#accueilBtn", function(){
            // Charge dans la div vide #Arborescence (user.html) la div #FilesPage (formulaires.html) qui reçoit l'arborescence
        $("#arborescence").load("../../../../public/formulaires.html #filesPage", function(){
            
                //Appelle la fonction createJSTree() qui génère l'arborescence depuis un tableau JSON envoyé par le serveur
            $(function () {
                    // Réalise la requête ajax à une url donnée
                $.ajax({
                    async: true,
                    type: "GET",
                    url: "http://localhost/cgi-bin/TheBox.exe/JSONTree",
                    dataType: "json",
                    success: function (json) {
                        createJSTree(json);
                    },
    
                    error: function (xhr, ajaxOptions, thrownError) {
                            // xhr = XMLHttpRequest : objet JS permettant l'échange de données via http
                        alert(xhr.status);
                        alert(thrownError);
                    }
                })
            });            
        });
            
                // Génère l'arborescence avec le tableau JSON envoyé par le serveur dans la div #FilesPage (formulaire.html)
            function createJSTree(jsondata) {            
                $('#filesPage').jstree({
                    'core': {
                        'data': jsondata
                    }
                });

                    // Ouvre la fenêtre #alert via un click droit sur un noeud
                $('#filesPage').on("contextmenu",this,function(event) {
                        // contextmenu = plugin jsTree permettant le click droit
                       
                    event.preventDefault();

                        // enregistre dans la variable Parent le dossier parent sélectionné
                    let Parent = $('#filesPage').jstree('get_selected',true);
                            // get_selected = - fonction jsTree qui retourne un tableau des noeuds sélectionnés;
                            //                - par défaut il est sur false = retourne seulement les Ids, true = retourne l'objet en entier 
                            
                        // ouvre la fenêtre
                    $( "#alert" ).dialog( "open" );
        
                     // Crée un nouveau dossier
                    $("#createFolderBtn").click( function(){
                            // #btncreateFolder = bouton "créer dossier" 

                        let newFolder = $("#folderName").val();
                            // #foldername = champ rempli par l'utilisateur, nom du nouveau dossier
                        
                        $('filesPage').jstree().create_node(
                            Parent, {                                       //créé un nouveau dossier avec certains parametres
                                "id": newFolder + "_id",
                                "text": newFolder,
                                    },  
                            "last", function() {
                                alert(newFolder +" créé");             // ouvre une fenêtre pour nous avertir de la création du dossier
                            }
                        );
                    })
                })
            }
    })
});



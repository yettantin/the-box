program TheBox;

{$APPTYPE CONSOLE}

uses
  Web.WebBroker,
  CGIApp,
  POCTheBox_Unit in 'POCTheBox_Unit.pas' {WebModuleTB: TWebModule};

{$R *.res}

begin
  Application.Initialize;
  Application.WebModuleClass := WebModuleClass;
  Application.Run;
end.
